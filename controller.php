<?php defined('C5_EXECUTE') or die("Access Denied.");

class ApiBasePackageUpdatesPackage extends Package {

	protected $pkgHandle = 'api_base_package_updates';
	protected $appVersionRequired = '5.6.0';
	protected $pkgVersion = '0.9';

	public function getPackageName() {
		return t("Api:Base:Package Updates");
	}

	public function getPackageDescription() {
		return t("Get Package Updates");
	}

	public function install() {
		$installed = Package::getByHandle('api');
		if(!is_object($installed)) {
			throw new Exception(t('Please install the "API" package before installing %s', $this->getPackageName()));
		}

		parent::install();

		$pkg = Package::getByHandle($this->pkgHandle);
		ApiRoute::add('packages/updates', t('Get available updates for installed packages'), $pkg);
	}
	
	public function uninstall() {
		ApiRouteList::removeByPackage($this->pkgHandle);//remove all the apis
		parent::uninstall();
	}

}