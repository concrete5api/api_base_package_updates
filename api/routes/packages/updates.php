<?php defined('C5_EXECUTE') or die('Access Denied');

class PackagesUpdatesApiRouteController extends ApiRouteController {

	public function run() {
		switch (API_REQUEST_METHOD) {
			case 'GET':
				return $this->getUpdates();
			
			default: //BAD REQUEST
				$this->setCode(400);
				$this->respond(array('error' => 'Bad Request'));
		}
	}

	public function getUpdates() {
		Loader::library('marketplace'); //bug in 5.6.0.1 and below in which the autoloader does not have the marketplace library loaded.
		$mi = Marketplace::getInstance();

		$pkgRemote = array();
		$pkgLocal = array();
		if(!ENABLE_MARKETPLACE_SUPPORT || !is_object($mi) || !$mi->isConnected()) {
			$this->setCode(501);
			$this->respond(array('error' => 'Marketplace Support Disabled'));
		}

		$pkgArray = Package::getInstalledList();
		foreach($pkgArray as $pkg) {
			if ($pkg->isPackageInstalled() && version_compare($pkg->getPackageVersion(), $pkg->getPackageVersionUpdateAvailable(), '<')) { 
				$pkg = $this->filterObject($pkg, array('pkgID', 'pkgName', 'pkgHandle', 'pkgVersion', 'pkgAvailableVersion'));
				$pkgRemote[] = $pkg;
			}
		}

		$pkgAvailableArray = Package::getLocalUpgradeablePackages();
		foreach($pkgAvailableArray as $pkg) {
			if (!in_array($pkg, $pkgRemote)) {
				$pkg = $this->filterObject($pkg, array('pkgID', 'pkgName', 'pkgHandle', 'pkgVersion', 'pkgAvailableVersion'));
				$pkgLocal[] = $pkg;
			}
		}

		$ret = array();
		$ret['local'] = $pkgLocal;
		$ret['remote'] = $pkgRemote;

		return $ret;
	}

}